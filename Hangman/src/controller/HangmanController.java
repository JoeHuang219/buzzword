package controller;

import apptemplate.AppTemplate;
import data.GameData;
import gui.Workspace;
import javafx.animation.*;
import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.util.Duration;
import javafx.util.Pair;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.beans.EventHandler;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;
import java.security.MessageDigest;

import static javafx.scene.input.KeyCode.CONTROL;
import static javafx.scene.input.KeyCode.ENTER;
import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 * @author Ritwik Banerjee
 * @author Joe Huang 109713061
 */
public class HangmanController implements FileController {

    public enum GameState {
        UNINITIALIZED,
        INITIALIZED_UNMODIFIED,
        INITIALIZED_MODIFIED,
        ENDED
    }

    private AppTemplate appTemplate; // shared reference to the application
    private GameData    gamedata;    // shared reference to the game being played, loaded or saved
    private GameState   gamestate;   // the state of the game being shown in the workspace
    private Text[]      progress;    // reference to the text area for the word
    private boolean     success;     // whether or not player was successful
    private int         discovered;  // the number of letters already discovered
    private Button      gameButton;  // shared reference to the "start game" button
    private Label       remains;     // dynamically updated label that indicates the number of remaining guesses
    private Path        workFile;
    private Text[]      allWords;
    private Canvas      canvas;
    private GraphicsContext gc;
    private boolean     yesHint;
    private Workspace   workspace;
    /////////////////////////////////////////////////////////////////////////
    public boolean     login;
    private boolean    gameOn = false;
    private boolean    gameStart = false;
    private ArrayList<String>   answerList;
    HashSet<String>     wordlist;
    private int         targetPoint;
    public int          currentLevel;
    public String       currentMode;
    private static final Integer STARTTIME = 15;
    private IntegerProperty timeSeconds;
    private char[][]    lettergrid;
    private ArrayList<String>   guessedList;
    Timeline timer;
    private Timeline       timeline;
    private int gameDuration;
    private IntegerProperty gameDurationInInteger;
    private String answer;
    private boolean[][] validChar;
    private ArrayList guessedChar;

    public HangmanController(AppTemplate appTemplate, Button gameButton) {
        this(appTemplate);
        this.gameButton = gameButton;
    }

    public HangmanController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        this.gamestate = GameState.UNINITIALIZED;
        initializeDictionary();
    }

    public void login(String username, String pw) throws Exception{
        if(!login) {
            gamedata = (GameData) appTemplate.getDataComponent();
            Path path = Paths.get("Hangman/saved/" + username + ".json");
            if (!Files.exists(path)) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show("ERROR", "Username or password is incorret, please try again");
            } else {
                appTemplate.getFileComponent().loadData(appTemplate.getDataComponent(), path);
                gamedata = (GameData) appTemplate.getDataComponent();

                MessageDigest md = MessageDigest.getInstance("MD5");
                md.update(pw.getBytes());
                byte[] digest = md.digest();
                StringBuffer keeper = new StringBuffer();

                for(byte b : digest){
                    keeper.append(String.format("%02x", b & 0xff));
                }

                if (keeper.toString().equals(gamedata.getPw())) {
                    // set the work file as the file from which the game was loaded
                    workFile = path;
                    Workspace gameworkspace = (Workspace) appTemplate.getWorkspaceComponent();
                    ensureActivatedWorkspace();
                    gameworkspace.NewHome();
                    login = true;
                    gameworkspace.disableCreate(true);
                    gameworkspace.setCurrentUser(username);
                    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                    dialog.show("Great", "You had successfully log in");
                } else {
                    gamedata.reset();
                    workFile = null ;
                    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                    dialog.show("Great", "Username or password is incorret, please try again");
                }
            }
        }else{
            gamedata.reset();
            login = false;
            workFile = null;
            Workspace gameworkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameworkspace.setCurrentUser("User not log in");
            gameworkspace.disableCreate(false);
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("Great", "You just log out.");
        }
    }
    public String getAnswer(){
        return answer;
    }
    public void setAnswer(String answer){
        this.answer = answer;
    }
    public void createProfile(String username, String pw) throws Exception{
        gamedata = (GameData) appTemplate.getDataComponent();
        File file = new File("Hangman/saved/"+ username + ".json");
        if(!file.exists()) {
            gamedata.setUsername(username);

            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(pw.getBytes());
            byte[] digest = md.digest();
            StringBuffer keeper = new StringBuffer();
            for(byte b : digest){
                keeper.append(String.format("%02x", b & 0xff));
            }
            gamedata.setPw(keeper.toString());
            gamedata.setMaxLevel(0);
            gamedata.setMmaxLevel(0);
            gamedata.setDmaxLevel(0);
            gamedata.initPersonalBest();
            for(int i = 0; i < 8; i++){
                gamedata.setePersonalBest(i,0);
                gamedata.setdPersonalBest(i,0);
                gamedata.setmPersonalBest(i,0);
            }
            Path path = file.toPath();
            appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), path);
            workFile = path;
            AppMessageDialogSingleton dialog     = AppMessageDialogSingleton.getSingleton();
            dialog.show("Great!","Great! \n You just set up your profile!");
        }
        else{
            AppMessageDialogSingleton dialog     = AppMessageDialogSingleton.getSingleton();
            dialog.show("ERROR","Username has already been used. Please try again!");
        }
    }
    public void updateHighScore()  throws Exception{
        if(workFile != null) {
            Path path = workFile;
            appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), path);
        }
        else{
            AppMessageDialogSingleton dialog     = AppMessageDialogSingleton.getSingleton();
            dialog.show("ERROR","Username has already been used. Please try again!");
        }

    }

    public void easyModeScreen(String mode){
        Workspace gameworkspace = (Workspace) appTemplate.getWorkspaceComponent();
        int level = 0;
        if(mode.equals("Easy")){
            level = gamedata.getMaxLevel();
        }
        else if(mode.equals("Medium")){
            level = gamedata.getMmaxLevel();
        }
        else{
            level = gamedata.getDmaxLevel();
        }
        gameworkspace.displayMode(level);
    }

    public char[][] setUpLetter(){
        String alphabet = "aaaaaaaabbcccddddeeeeeeeeeeeeeffgghhhhhhiiiiiiijkllllmmnnnnnnnooooooooppqrrrrrrsssssstttttttttuuuvwwxyyz";

        Random r = new Random();

        char[][] lettergrid = new char[4][4];

        for(int i =0; i < 4; i++){
            for(int j = 0; j < 4; j++){
                lettergrid[i][j] = alphabet.charAt(r.nextInt(alphabet.length()));
            }
        }
        return lettergrid;
    }

    public int getAnswerListSize(){
        return answerList.size();
    }

    public int getTargetPoint(){
        return targetPoint;
    }

    public boolean checkCurrentGame(){
        if(gameOn){
            return true;
        }
        return false;
    }

    public Timeline getTimer(){
        return timer;
    }

    public void setGameOn(boolean on){
        gameOn = on;
    }

    public void setGameStart(boolean on){
        gameStart = on;
    }

    public boolean getGameStart(){
        return gameStart;
    }


    public boolean askToExit(){
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();
        yesNoCancelDialog.show("STOP",
                "The game is still going. \nAre you sure you want to exit the application?");
        if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES))
            return true;
        return false;
    }

    public char[][] findWords(char[][] lettergrid){
        answerList = new ArrayList<>();
        System.out.println("CHEAT SHEET: ");
        while(answerList.isEmpty()) {
            for(String k : wordlist){
                if(check(k.toLowerCase(),lettergrid) && !answerList.contains(k)){
                    answerList.add(k);
                }
            }
//            for (int i = 0; i < 4; i++) {
//                for (int j = 0; j < 4; j++) {
//                    String answer = "";
//                    int x = i;
//                    int y = j;
//                    boolean[][] check = new boolean[4][4];
//                    for (int k = 0; k < 4; k++) {
//                        for (int z = 0; z < 4; z++) {
//                            check[k][z] = false;
//                        }
//                    }
//                    DFS(check, lettergrid, x, y, answer);
//                }
//            }
            if(answerList.isEmpty()){
                lettergrid = setUpLetter();
                System.out.println("reset");
            }
        }
        this.lettergrid = lettergrid;
        int temp = 0;
        double difficultyPoint= 0.1 + currentLevel * 0.025;
        for(int i = 0; i < answerList.size(); i++){
            temp += answerList.get(i).length()*2;
        }
        this.targetPoint = (int) (difficultyPoint * temp);

        return lettergrid;
    }


    public boolean check(final String word, char[][] lettergrid) {
        int diff;
        if(currentMode.equals("Easy")){
            diff = 3;
        }
        else if(currentMode.equals("Medium")){
            diff = 4;
        }
        else{
            diff = 5;
        }

        if(word.length() < diff){
            return false;
        }
        //check neighboring elements
        int[] dx = {1, 0, -1, 0,1,1,-1,-1};
        int[] dy = {0, 1, 0, -1,1,-1,1,-1};

        boolean[][] visited = new boolean[4][4];

        boolean[][][] matrix = new boolean[100][4][4];
        char[] letters = word.toCharArray();

        for(int i = 0; i < 4; i++){
            for(int j = 0; j <4; j++){
                visited[i][j] = false;
            }
        }

        for(int z = 0; z < word.length();z++){
            for(int i = 0; i < 4; i++){
                for(int j = 0; j < 4; j++){
                    if (z == 0 ) {
                        if(lettergrid[i][j] == letters[z]) {
                            matrix[z][i][j] = true;
                            visited[i][j] = true;
                        }
                    }
                    else {
                        for (int neighbor = 0; neighbor < 8; neighbor++) {
                            int x = i + dx[neighbor];
                            int y = j + dy[neighbor];

                            if ((x >= 0) && (x < 4) && (y >= 0) && (y < 4)
                                    && (matrix[z - 1][x][y]) && (lettergrid[i][j] == letters[z]) && !visited[i][j]) {
                                matrix[z][i][j] = true;
                                visited[i][j] = true;
                                if (z == letters.length-1) {

                                    System.out.println(word);
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    public void changePw(String pw, String newPw) throws Exception{
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(pw.getBytes());
        byte[] digest = md.digest();
        StringBuffer keeper = new StringBuffer();
        for(byte b : digest){
            keeper.append(String.format("%02x", b & 0xff));
        }
        if(gamedata.getPw().equals(keeper.toString())){
            md = MessageDigest.getInstance("MD5");
            md.update(newPw.getBytes());
            digest = md.digest();
            keeper = new StringBuffer();
            for(byte b : digest){
                keeper.append(String.format("%02x", b & 0xff));
            }
            gamedata.setPw(keeper.toString());
            updateHighScore();
            AppMessageDialogSingleton dialog     = AppMessageDialogSingleton.getSingleton();
            dialog.show("CONGRATZ","YOU SET A NEW PASSWORD");
        }else{
            //pop
            AppMessageDialogSingleton dialog     = AppMessageDialogSingleton.getSingleton();
            dialog.show("Wrong","Wrong password! Try again later.");
        }
    }


    public void shortCut(){

        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        DropShadow dropShadow = new DropShadow();

        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) ->{
            if(event.getCharacter().charAt(0) == 17){
                boolean check = getGameStart();
                if (check) {
                    boolean exitGame = askToExit();
                    if(exitGame){
                        Platform.exit();
                    }
                } else {
                    Platform.exit();
                }
            }
            if(event.getCharacter().charAt(0) == 8){
                gameWorkspace.OldHome();
            }
            if(event.getCharacter().charAt(0) == 12){
                if(!login) {
                    gameWorkspace.Login();
                }
                else{
                    try {
                        login(null,null);
                        gameWorkspace.OldHome();
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            }

        });
    }

    public void initializeDictionary(){
//        URL wordsResource = getClass().getClassLoader().getResource("words/words.txt");
//        assert wordsResource != null;
//
//        wordlist = new HashSet<>();
//        for(int i = 0; i < 19912;i++) {
//            try (Stream<String> lines = Files.lines(Paths.get(wordsResource.toURI()))) {
//                wordlist.add(lines.skip(i).findFirst().get());
//            } catch (IOException | URISyntaxException e) {
//                e.printStackTrace();
//                System.exit(1);
//            }
//        }
        File dictionary = new File("Hangman/resources/words/3letters.txt");
        wordlist = new HashSet<>();
        try {
            Scanner input = new Scanner(dictionary);
            while(input.hasNext()){
                wordlist.add(input.next());
            }
        }catch(FileNotFoundException e){
            System.out.print("File not found");
            System.exit(1);
        }
    }
    public ArrayList<String> getAnswerList(){
        return answerList;
    }
    public boolean getGameOn(){
        return gameOn;
    }
    public ArrayList<String> getGuessedList(){
        return guessedList;
    }
    public void clearButton(){
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                gameWorkspace.getButtons()[i][j].setEffect(null);
            }
        }
    }
    public void play(){
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();

        if(gameStart) {
            guessedChar = new ArrayList();
            answer = "";
            guessedList = new ArrayList<>();
        }

//        timer = new Timeline();
//        timer.getKeyFrames().set
//        {
//
//
//            public void handle(long now) {
        DropShadow dropShadow= new DropShadow();
        dropShadow.setRadius(10.0);
        dropShadow.setOffsetX(3.0);
        dropShadow.setOffsetY(3.0);
        dropShadow.setColor(Color.RED);

        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) ->{
            if(event.getCharacter().charAt(0) == 17){
                boolean check = getGameStart();
                if (check) {
                    boolean exitGame = askToExit();
                    if(exitGame){
                        Platform.exit();
                    }
                } else {
                    Platform.exit();
                }
            }
            if(gameStart && gameOn) {
                if (event.getCharacter().charAt(0) == 13) {
                    for (int i = guessedChar.size() - 1; i >= 0; i--) {
                        guessedChar.remove(i);
                    }
                    if (answerList.contains(answer) && !guessedList.contains(answer)) {
                        guessedList.add(answer);
                        gameWorkspace.getGuessedWordBox().appendText(
                                answer + "\t" + Integer.toString(answer.length() * 2) + "\n");

                        gameWorkspace.addCurrentPoint(answer.length() * 2);
                    }
                    answer = "";
                    for (int i = 0; i < 42; i++) {
                        gameWorkspace.getLine()[i].setEffect(null);
                    }
                    for (int i = 0; i < 4; i++) {
                        for (int j = 0; j < 4; j++) {
                            gameWorkspace.getButtons()[i][j].setEffect(null);
                        }
                    }
                    gameWorkspace.setGuessingLabel("");

                } else {
                    answer += event.getCharacter().charAt(0);
                    boolean exist = false;
                    for (int i = 0; i < lettergrid.length; i++) {
                        for (int j = 0; j < lettergrid[i].length; j++) {
                            if (lettergrid[i][j] == event.getCharacter().charAt(0)) {
                                if (!exist) {
                                    String gg = gameWorkspace.getGuessingLabel();
                                    gg += event.getCharacter().charAt(0);
                                    gameWorkspace.setGuessingLabel(gg);
                                    exist = true;
                                    if (!guessedChar.contains(event.getCharacter().charAt(0))) {
                                        guessedChar.add(event.getCharacter().charAt(0));
                                    }
                                }
//                                        boolean checkCont = false;
//                                        int[] hx = new int[]{-1,-1,-1,0,0,1,1,1};
//                                        int[] hy = new int[]{-1,0,1,-1,1,-1,0,1};
//                                        for(int k = 0; k < hx.length; k++){
//                                                int x = hx[k];
//                                                int y = hy[k];
//                                                if(i+x >= 0 && i +x < 4 && j+y >= 0 && j + y < 4) {
//                                                    if (guessedChar.contains(lettergrid[i + x][j + y])) {
//                                                        checkCont = true;
//                                                    }
//                                                }
//
//                                        }
//                                        if(checkCont || guessedChar.size() == 1) {
                                boolean[][] checker = checkHighLighter();
                                dfsMain();
                                highLighter(event.getCharacter().charAt(0), guessedChar, validChar);
                            }
                        }
                    }
                }
            }
            else{

            }
                    });

            }
//            @Override
//            public void stop(){
//                super.stop();
//            }
//        };
//        timer.play();
    public void dfsMain(){
        validChar = new boolean[4][4];
        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 4 ; j++){
//                boolean[][] visited = new boolean[4][4];
                boolean[][] tempYes = new boolean[4][4];
//
                for(int k = 0; k < 4; k++){
                    for(int z =0; z < 4; z++){
                        tempYes[k][z] = false;
//                        visited[k][z] = false;
                    }
                }
//                dfs(tempYes,i,j,0,answer.length(),visited);
                bt(0,answer.length(),i,j,tempYes);
            }
        }
    }
    public void bt(int pos, int length,int x, int y,  boolean[][] p){
        p[x][y] = true;
        if(pos == length){
            for(int i = 0; i < 4; i++){
                for(int j = 0; j <4; j++){
                    if(p[i][j]){
                        validChar[i][j] = true;
                    }
                }
            }
        }
        else{

            int[] hx = new int[]{-1, -1, -1, 0, 0, 1, 1, 1};
            int[] hy = new int[]{-1, 0, 1, -1, 1, -1, 0, 1};

            for (int w = 0; w < 8; w++) {
                if (x + hx[w] >= 0 && x + hx[w] < 4 && y + hy[w] >= 0 && y + hy[w] < 4
                        && !p[x + hx[w]][y + hy[w]]) {
                    if(lettergrid[x + hx[w]][y + hy[w]]==answer.charAt(pos)){
                        bt(pos+1,length,x+hx[w],y+hy[w],p);
                    }
                }
            }
        }


    }
    public void dfs(boolean[][] p, int x, int y, int pos, int v, boolean[][] visited){

        if(pos == v){
            for(int i = 0; i < 4;i++){
                for(int j = 0; j < 4; j++){
                    if(p[i][j]){
                        validChar[i][j] = true;
                    }
                }
            }

        }
        else{
            visited[x][y] = true;
            if(lettergrid[x][y] != answer.charAt(pos)){

            }
            else{
                p[x][y] = true;
                int[] hx = new int[]{-1, -1, -1, 0, 0, 1, 1, 1};
                int[] hy = new int[]{-1, 0, 1, -1, 1, -1, 0, 1};


                for (int w = 0; w < 8; w++) {
                    if (x + hx[w] >= 0 && x + hx[w] < 4 && y + hy[w] >= 0 && y + hy[w] < 4
                            && !visited[x + hx[w]][y + hy[w]]) {
                        dfs(p,x+hx[w],y+hy[w],pos+1,v,visited);
                    }
                }
            }
        }
    }


    public boolean[][] checkHighLighter(){
        int length = answer.length();
        boolean[][] yes = new boolean[4][4];
        for(int i = 0; i < 4; i++){
            for(int j =0; j < 4; j++){
                yes[i][j] = false;
            }
        }
        validChar = new boolean[4][4];
//        backtracking(yes,0,length,0,0,visited);
        Queue queue = new LinkedList();
        for(int i = 0; i < lettergrid.length; i++){
            for(int j = 0; j < lettergrid[i].length; j++){
                int checkLength = 0;
                boolean[][] tempYes = new boolean[4][4];
                boolean[][] visited = new boolean[4][4];

                for(int k = 0; k < 4; k++){
                    for(int z =0; z < 4; z++){
                        tempYes[k][z] = false;
                        visited[k][z] = false;
                    }
                }
                Pair loc = new Pair(i,j);
                queue.add(loc);
                while(!queue.isEmpty()){
                    Pair temp = (Pair) queue.remove();
                    int x = (int)temp.getKey();
                    int y = (int)temp.getValue();
                    if( checkLength > 0 && lettergrid[x][y] == answer.charAt(checkLength-1)){
                        checkLength--;
                    }
                    if(checkLength < answer.length() && lettergrid[x][y] == answer.charAt(checkLength)){
                        checkLength++;
                        tempYes[x][y] = true;
                        visited[x][y] = true;
                        if(checkLength != length) {
                            int[] hx = new int[]{-1, -1, -1, 0, 0, 1, 1, 1};
                            int[] hy = new int[]{-1, 0, 1, -1, 1, -1, 0, 1};

                            for (int w = 0; w < 8; w++) {
                                if (x + hx[w] >= 0 && x + hx[w] < 4 && y + hy[w] >= 0 && y + hy[w] < 4
                                        && !visited[x + hx[w]][y + hy[w]]) {
                                    loc = new Pair(x + hx[w], y + hy[w]);
                                    queue.add(loc);
                                }
                            }
                        }
                    }
                }
                if(checkLength == length){
                    for(int p = 0; p <4; p++){
                        for(int q = 0; q < 4; q++){
                            if(tempYes[p][q]) {
                                yes[p][q] = true;
                            }
                        }
                    }
                }
            }
        }
        return yes;
    }
//    public void backtracking(boolean[][] p, int pos, int v, int x, int y, boolean[][] visited){
//        visited[x][y] = true;
//        if(pos == v){
//            for(int i = 0; i < 4; i ++) {
//                for (int j = 0; j < 4; j++) {
//                    boolean valid = false;
//                    for(int k = 0; k < v; k++){
//                        if(answer.charAt(k) == lettergrid[i][j]){
//                            valid = true;
//                        }
//                    }
//                    if(valid){
//                        if(p[i][j]){
//                            validChar[i][j]= true;
//                        }
//                    }
//                }
//            }
//        }
//        else{
//            int[] hx = new int[]{-1, -1, -1, 0, 0, 1, 1, 1};
//            int[] hy = new int[]{-1, 0, 1, -1, 1, -1, 0, 1};
////            boolean end = true;
////            for (int w = 0; w < 8; w++) {
////                if (x + hx[w] >= 0 && x + hx[w] < 4 && y + hy[w] >= 0 && y + hy[w] < 4
////                        && !visited[x + hx[w]][y + hy[w]]) {
////                    end = false;
////                }
////            }
////            if(end){
////                return;
////            }
//
//            if(answer.charAt(pos) == lettergrid[x][y]) {
//                p[x][y] = true;
//                pos++;
//            }
//            for (int w = 0; w < 8; w++) {
//                if (x + hx[w] >= 0 && x + hx[w] < 4 && y + hy[w] >= 0 && y + hy[w] < 4
//                        && !visited[x + hx[w]][y + hy[w]]) {
//                    backtracking(p,pos,v,x +hx[w],y+hy[w],visited);
//                }
//
//            }
//
//        }
//
//
//
//    }
    public void highLighter(char c, ArrayList guessedChar, boolean[][] checker){
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();

        for(int i =0; i < gameWorkspace.getButtons().length; i++){
            for(int j = 0; j < gameWorkspace.getButtons()[i].length; j++){
                if(!checker[i][j]) {
                    gameWorkspace.getButtons()[i][j].setEffect(null);
                }
            }
        }
        for(int i = 0; i < 42; i++){
            gameWorkspace.getLine()[i].setEffect(null);
        }

        for(int i = 0; i < 3; i++){
            for( int j = 0; j < 3; j++){
                if(checker[i][j] && checker[i][j+1]){
                    findtheLine(i,j,i,j+1);

                }
                if(checker[i][j] && checker[i+1][j]){
                    findtheLine(i,j,i+1,j);

                }
                if(checker[i][j] && checker[i+1][j+1]){
                    findtheLine(i,j,i+1,j+1);
                }

            }
        }
        for(int i = 1; i < 4; i++){
            for(int j = 1; j < 4; j++){
                if(checker[i][j] && checker[i-1][j-1]){
                    findtheLine(i,j,i-1,j-1);
                }
                if(checker[i][j] && checker[i-1][j]){
                    findtheLine(i,j,i-1,j);
                }
                if(checker[i][j] && checker[i][j-1]){
                    findtheLine(i,j,i,j-1);
                }
            }
        }

        DropShadow dropShadow= new DropShadow();
        dropShadow.setRadius(10.0);
        dropShadow.setOffsetX(3.0);
        dropShadow.setOffsetY(3.0);
        dropShadow.setColor(Color.RED);
        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 4; j++) {
                if (lettergrid[i][j] == c && checker[i][j]) {

                    gameWorkspace.getButtons()[i][j].setEffect(dropShadow);
                    //check around
                    int lineNumber;
                    if(i-1 >= 0 && j-1 >= 0 && guessedChar.contains(lettergrid[i-1][j-1]) && checker[i-1][j-1]){
                        lineNumber = findtheLine(i,j,i-1,j-1);
                        gameWorkspace.getLine()[lineNumber].setEffect(dropShadow);
                        gameWorkspace.getButtons()[i-1][j-1].setEffect(dropShadow);
                    }
                    if(i-1 >= 0 && guessedChar.contains(lettergrid[i-1][j]) && checker[i-1][j]){
                        lineNumber =findtheLine(i,j,i-1,j);
                        gameWorkspace.getLine()[lineNumber].setEffect(dropShadow);
                        gameWorkspace.getButtons()[i-1][j].setEffect(dropShadow);

                    }
                    if(i-1 >= 0 && j+1 < 4 && guessedChar.contains(lettergrid[i-1][j+1]) && checker[i-1][j+1]){
                        lineNumber =findtheLine(i,j,i-1,j+1);
                        gameWorkspace.getLine()[lineNumber].setEffect(dropShadow);
                        gameWorkspace.getButtons()[i-1][j+1].setEffect(dropShadow);

                    }
                    if( j-1 >= 0 && guessedChar.contains(lettergrid[i][j-1]) && checker[i][j-1]){
                        lineNumber =findtheLine(i,j,i,j-1);
                        gameWorkspace.getLine()[lineNumber].setEffect(dropShadow);
                        gameWorkspace.getButtons()[i][j-1].setEffect(dropShadow);

                    }
                    if(j+1 < 4 && guessedChar.contains(lettergrid[i][j+1]) && checker[i][j+1]){
                        lineNumber =findtheLine(i,j,i,j+1);
                        gameWorkspace.getLine()[lineNumber].setEffect(dropShadow);
                        gameWorkspace.getButtons()[i][j+1].setEffect(dropShadow);

                    }
                    if(i+1 < 4 && j-1 >= 0 && guessedChar.contains(lettergrid[i+1][j-1]) && checker[i+1][j-1]){
                        lineNumber =findtheLine(i,j,i+1,j-1);
                        gameWorkspace.getLine()[lineNumber].setEffect(dropShadow);
                        gameWorkspace.getButtons()[i+1][j-1].setEffect(dropShadow);

                    }
                    if(i+1 < 4 && guessedChar.contains(lettergrid[i+1][j]) && checker[i+1][j]){
                        lineNumber =findtheLine(i,j,i+1,j);
                        gameWorkspace.getLine()[lineNumber].setEffect(dropShadow);
                        gameWorkspace.getButtons()[i+1][j].setEffect(dropShadow);

                    }
                    if(i + 1 < 4 && j+ 1 < 4 && guessedChar.contains(lettergrid[i+1][j+1]) && checker[i+1][j+1]){
                        lineNumber = findtheLine(i,j,i+1,j+1);
                        gameWorkspace.getLine()[lineNumber].setEffect(dropShadow);
                        gameWorkspace.getButtons()[i][j].setEffect(dropShadow);
                        gameWorkspace.getButtons()[i+1][j+1].setEffect(dropShadow);

                    }
                }
            }
        }
    }

    public int findtheLine(int i, int j, int x, int y){
        if(i == x){
            int temp = 0;
            switch(i){
                case 0: temp = 0;break;
                case 1: temp = 1; break;
                case 2: temp = 2; break;
                case 3: temp = 3; break;
            }
            int diff;
            if(j > y) diff = y;
            else diff = j;

            switch(diff){
                case 0: temp += 0; break;
                case 1: temp += 4; break;
                case 2: temp += 8; break;
            }

            return temp;
        }
        if(j == y){
            //12 ~ 23
            int temp = 12;
            switch(j){
                case 0: break;
                case 1: temp += 3;
                    break;
                case 2: temp = 18; break;
                case 3:temp = 21; break;
            }

            int diff = (x > i? i:x);
            switch(diff){
                case 0: break;
                case 1: temp++;
                    break;
                case 2: temp += 2;
                    break;
                case 3:temp +=3;
                    break;
            }
            return temp;
        }
        else{
            if( i > x && j > y){
                int temp = 24;
                switch(j){
                    case 2: temp+=3; break;
                    case 3: temp += 6; break;
                }
                switch(i){
                    case 2: temp++;
                        break;
                    case 3: temp+=2; break;
                }
                return temp;
            }
            else if(i < x && j < y){
                int temp = 24;
                switch(y){
                    case 2: temp+=3; break;
                    case 3: temp += 6; break;
                }
                switch(x){
                    case 2: temp++;
                        break;
                    case 3: temp+=2; break;
                }
                return temp;
            }
            else if(i < x && j > y){
                int temp = 33;
                switch(j){
                    case 2: temp +=3; break;
                    case 3: temp +=6; break;
                }
                switch(i){
                    case 1: temp +=1; break;
                    case 2: temp +=2; break;
                }
                return temp;
            }
            else{
                int temp = 33;
                switch(y){
                    case 2: temp +=3; break;
                    case 3: temp +=6; break;
                }
                switch(x){
                    case 1: temp +=1; break;
                    case 2: temp +=2; break;
                }
                return temp;
            }
        }
    }

    public void gameOver(){
        Workspace gameworkspace = (Workspace) appTemplate.getWorkspaceComponent();

        for(int i =0; i < gameworkspace.getButtons().length; i++){
            for(int j = 0; j < gameworkspace.getButtons()[i].length; j++){
                gameworkspace.getButtons()[i][j].setEffect(null);

            }
        }
        for(int i = 0; i < 42; i++){
            gameworkspace.getLine()[i].setEffect(null);
        }

        gameworkspace.getGuessedWordBox().setMaxHeight(90);
        TextArea tempAnswer = new TextArea();
        gameworkspace.getSmallGuessedBox().getChildren().add(tempAnswer);
        tempAnswer.setMaxHeight(95);
        tempAnswer.setMaxWidth(140);
        tempAnswer.setStyle("-fx-text-fill: RED");
        for(int i = 0; i < answerList.size(); i++){
            if(!guessedList.contains(answerList.get(i))){
                Text newPoint = new  Text( answerList.get(i)+"\t" + Integer.toString(answerList.get(i).length()*2)+"\n");
                newPoint.setFill(Color.RED);
                tempAnswer.appendText(
                        answerList.get(i)+"\t" + "\t" +Integer.toString(answerList.get(i).length()*2)+"\n"
                );


            }
        }
        int best;
        boolean pass = false;
        if(currentMode.equals("Easy")){
            if(gameworkspace.getCurrentPoint() > targetPoint){
                int leve = gamedata.getMaxLevel();
                gamedata.setMaxLevel(++leve);
                pass = true;
            }

            best = gamedata.getePersonalBest(currentLevel-1);

        }
        else if(currentMode.equals("Medium")){
            if(gameworkspace.getCurrentPoint() > targetPoint){
                int leve = gamedata.getMmaxLevel();
                gamedata.setMmaxLevel(++leve);
                pass = true;

            }
            best  = gamedata.getmPersonalBest()[currentLevel-1];        }
        else{
            if(gameworkspace.getCurrentPoint() > targetPoint){
                int leve = gamedata.getDmaxLevel();
                gamedata.setDmaxLevel(++leve);
                pass = true;
            }
            best  = gamedata.getdPersonalBest()[currentLevel-1];        }

        if(gameworkspace.getCurrentPoint() > best){
            if(currentMode.equals("Easy")){
                gamedata.setePersonalBest(currentLevel-1,gameworkspace.getCurrentPoint());
            }
            else if(currentMode.equals("Medium")){
                gamedata.setmPersonalBest(currentLevel-1,gameworkspace.getCurrentPoint());
            }
            else{
                gamedata.setdPersonalBest(currentLevel-1,gameworkspace.getCurrentPoint());
            }
            try {
                updateHighScore();
            }catch(Exception e1){

            }
            gameworkspace.addPersonalBest(gameworkspace.getCurrentPoint());

            if(pass){
                gameworkspace.goToNext();
                gameworkspace.goSave();
            }

        }

    }


    public String getUsername(){
        return gamedata.getUsername();
    }
    public void setCurrentMode(String mode){
        currentMode = mode;
    }
    public void setCurrentLevel(){ currentLevel++;}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void enableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(false);
    }

    public void disableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(true);
    }

    public void setGameState(GameState gamestate) {
        this.gamestate = gamestate;
    }



    /**
     * In the homework code given to you, we had the line
     * gamedata = new GameData(appTemplate, true);
     * This meant that the 'gamedata' variable had access to the app, but the data component of the app was still
     * the empty game data! What we need is to change this so that our 'gamedata' refers to the data component of
     * the app, instead of being a new object of type GameData. There are several ways of doing this. One of which
     * is to write (and use) the GameData#init() method.
     *
     */


    private void end() {
        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
        gameButton.setDisable(true);
        setGameState(GameState.ENDED);

        yesHint = false;
        appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));
        Platform.runLater(() -> {
            PropertyManager           manager    = PropertyManager.getManager();
            AppMessageDialogSingleton dialog     = AppMessageDialogSingleton.getSingleton();
            String                    endMessage = manager.getPropertyValue(success ? GAME_WON_MESSAGE : GAME_LOST_MESSAGE);
            if (!success){
                for(int i =0; i < progress.length; i++){
                    if(!progress[i].isVisible()){
                        progress[i].setVisible(true);
                        progress[i].setStyle("-fx-stroke: red");
                    }
                }
            }
            else{
                dialog.show("CONGRATZ", "YOU WIN!");
//                dialog.close();
            }

//            if (dialog.isShowing())
//                dialog.toFront();
//            else
//                dialog.show(manager.getPropertyValue(GAME_OVER_TITLE), endMessage);
        });
    }




    private void drawHangman(int badguess){
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setStroke(Color.BLACK);

        switch(badguess){
            case 1: gc.fillRoundRect(0,200,200,2,20,20);
                break;
            case 2: gc.fillRoundRect(0,0,2,200,20,20);
                break;
            case 3: gc.fillRoundRect(0,0,100,2,20,20);
                break;
            case 4: gc.fillRoundRect(100,0,2,50,20,20);
                break;
            case 5: gc.strokeOval(80,50,40,40);
                break;
            case 6: gc.strokeLine(100,90,100,120);
                break;
            case 7: gc.strokeLine(100,90,75,120);
                break;
            case 8: gc.strokeLine(100,90,125,120);
                break;
            case 9: gc.strokeLine(100,120,80,150);
                break;
            case 10: gc.strokeLine(100,120,120,150);
                break;
        }
    }


    @Override
    public void handleNewRequest() {
        AppMessageDialogSingleton messageDialog   = AppMessageDialogSingleton.getSingleton();
        PropertyManager           propertyManager = PropertyManager.getManager();
        boolean                   makenew         = true;
        if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
            try {
                makenew = promptToSave();
            } catch (IOException e) {
                messageDialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE), propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
            }
        if (makenew) {
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            workFile = null;                                       // new workspace has never been saved to a file
            ((Workspace) appTemplate.getWorkspaceComponent()).reinitialize();
            enableGameButton();
        }
        if (gamestate.equals(GameState.ENDED)) {
            appTemplate.getGUI().updateWorkspaceToolbar(false);
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
        }

    }

    @Override
    public void handleSaveRequest() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        if (workFile == null) {
            FileChooser filechooser = new FileChooser();
            Path        appDirPath  = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path        targetPath  = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
            ExtensionFilter extFilter = new ExtensionFilter(String.format("%s (*.%s)", description, extension),
                                                            String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showSaveDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null)
                save(selectedFile.toPath());
        } else
            save(workFile);
    }

    @Override
    public void handleLoadRequest() throws IOException {
        boolean load = true;
        if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
            load = promptToSave();
        if (load) {
            PropertyManager propertyManager = PropertyManager.getManager();
            FileChooser     filechooser     = new FileChooser();
            Path            appDirPath      = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path            targetPath      = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(LOAD_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
            ExtensionFilter extFilter = new ExtensionFilter(String.format("%s (*.%s)", description, extension),
                                                            String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showOpenDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null && selectedFile.exists())
                load(selectedFile.toPath());

        }
    }

    @Override
    public void handleExitRequest() {
        try {
            boolean exit = true;
            if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
                exit = promptToSave();
            if (exit)
                System.exit(0);
        } catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager           props  = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(SAVE_ERROR_TITLE), props.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }

    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }

    private boolean promptToSave() throws IOException {
        PropertyManager            propertyManager   = PropertyManager.getManager();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();

        yesNoCancelDialog.show(propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE),
                               propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));

        if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES))
            handleSaveRequest();

        return !yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.CANCEL);
    }

    /**
     * A helper method to save work. It saves the work, marks the current work file as saved, notifies the user, and
     * updates the appropriate controls in the user interface
     *
     * @param target The file to which the work will be saved.
     * @throws IOException
     */
    private void save(Path target) throws IOException {
        appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), target);
        workFile = target;
        setGameState(GameState.INITIALIZED_UNMODIFIED);
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE));
    }

    /**
     * A helper method to load saved game data. It loads the game data, notified the user, and then updates the GUI to
     * reflect the correct state of the game.
     *
     * @param source The source data file from which the game is loaded.
     * @throws IOException
     */
    private void load(Path source) throws IOException {
        // load game data
        appTemplate.getFileComponent().loadData(appTemplate.getDataComponent(), source);

        // set the work file as the file from which the game was loaded
        workFile = source;

        // notify the user that load was successful
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(LOAD_COMPLETED_TITLE), props.getPropertyValue(LOAD_COMPLETED_MESSAGE));

        setGameState(GameState.INITIALIZED_UNMODIFIED);
        Workspace gameworkspace = (Workspace) appTemplate.getWorkspaceComponent();
        ensureActivatedWorkspace();
        gameworkspace.reinitialize();
        gamedata = (GameData) appTemplate.getDataComponent();
    }
}
