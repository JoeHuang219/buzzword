package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.HangmanController;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ObservableIntegerValue;
import javafx.beans.value.ObservableValue;
import javafx.beans.value.WritableIntegerValue;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
import propertymanager.PropertyManager;
import ui.AppGUI;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.beans.EventHandler;
import java.io.IOException;
import java.util.EventListener;

import static hangman.HangmanProperties.*;

/**
 * This class serves as the GUI component for the Hangman game.
 *
 * @author Ritwik Banerjee
 *  @author Joe Huang 109713061
 */
public class Workspace extends AppWorkspaceComponent {

    AppTemplate app; // the actual application
    AppGUI      gui; // the GUI inside which the application sits

    Label             guiHeadingLabel;   // workspace (GUI) heading label
    HBox              headPane;          // conatainer to display the heading
    HBox              bodyPane;          // container for the main game displays
    ToolBar           footToolbar;       // toolbar for game buttons
    BorderPane        figurePane;        // container to display the namesake graphic of the (potentially) hanging person
    VBox              gameTextsPane;     // container to display the text-related parts of the game
    HBox              guessedLetters;    // text area displaying all the letters guessed so far
    HBox              remainingGuessBox; // container to display the number of remaining guesses
    Button            startGame;         // the button to start playing a game of Hangman
    HangmanController controller;
    StackPane[]       wordCircle;
    GridPane        letterPane;
    //***********************************************************//
    Button            test;
    Button            create;
    TextField         userinput;
    TextField         userPw;
    Button            closeLogin;
    Button            closeCreate;
    Button            closeProfile;
    Stage             loginInfo;
    Button            playerProfile;
    Menu              selectMode;
    Button            start;
    Pane              sidePane;
    Button            home;
    Button            currentUser;
    Pane              mainPane;
    Button            quit;
    BorderPane        appPane;
    Button            level;
    Button            Pause;
    Text              randomLetter;
    char[][]          lettergrid;
    Text              displayLetter;
    Label             targetPointLabel;
    Label             remainingTime;
    MenuItem          easy;
    MenuItem          medium;
    MenuItem          difficult;
    MenuBar           mainmenu;
    Button            modePage;
    Button            viewHelp;
    Label             counter;
    TextArea            guessedWordBox;
    private Timeline       timeline;
    private int gameDuration;
    private IntegerProperty gameDurationInInteger;
    Text              guessingLabel;
    Line[]            line;
    Button[][]        buttons;
    Label             currentPoint;
    VBox smallGuessedBox;
    Button            toNext;
    Button            replay;
    Button            editProfile;
    int pos;
    int posyy;
    boolean[][]       checkPass;
    Button            save;
    boolean[][]           startDrag = new boolean[4][4];
    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     * @throws IOException Thrown should there be an error loading application
     *                     data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
        app = initApp;
        gui = app.getGUI();
        controller = (HangmanController) gui.getFileController();    //new HangmanController(app, startGame); <-- THIS WAS A MAJOR BUG!??
        layoutGUI();     // initialize all the workspace (GUI) components including the containers and their layout
        setupHandlers(); // ... and set up event handling

    }

    public void setRemainingTime(String text){ remainingTime.setText(text);}
    public Label getCounter(){
        return counter;
    }
    public Button getCurrentUser(){
        return currentUser;
    }

    public Text getRandomLetter(){
        return randomLetter;
    }

    public void setCurrentUser(String currentUser){
        this.currentUser.setText(currentUser);
    }

    public Label getRemainingTime(){
        return remainingTime;
    }

    public Line[] getLine(){
        return line;
    }

    public void addCurrentPoint(int k){
        currentPoint.setText(Integer.toString(Integer.parseInt(currentPoint.getText()) + k));
    }
    public void zeroCurrentPoint(){
        currentPoint.setText("");
    }

    public Button getToNext(){
        return toNext;
    }

    public int getCurrentPoint(){
        return Integer.parseInt(currentPoint.getText());
    }
    public Button getReplay(){
        return replay;
    }
    public void goToReplay(){
        mainPane.getChildren().add(replay);
    }
    public void goToNext(){
        mainPane.getChildren().add(toNext);
    }
    public void goSave(){mainPane.getChildren().add(save);}
    public TextArea getGuessedWordBox(){
        return guessedWordBox;
    }
    public VBox getSmallGuessedBox(){
        return smallGuessedBox;
    }
    public Pane getMainPane(){
        return mainPane;
    }
    public Button[][] getButtons(){ return buttons;}
    public void setGuessingLabel(String gg){
        this.guessingLabel.setText(gg);
    }
    public String getGuessingLabel(){
        return guessingLabel.getText();
    }
    public Button getPause(){
        return Pause;
    }

    public void disableCreate(boolean disable){
        create.setDisable(disable);
    }
    private void layoutGUI() {
        PropertyManager propertyManager = PropertyManager.getManager();
        guiHeadingLabel = new Label("BUZZWORD");
        guiHeadingLabel.setMinWidth(178);
        guiHeadingLabel.setMinHeight(80);
        guiHeadingLabel.setFont(Font.font("Arial", 32));
        guiHeadingLabel.setAlignment(Pos.CENTER);
        currentUser = new Button("User not log in");
        currentUser.setDisable(true);
        currentUser.setMinWidth(178);
        currentUser.setAlignment(Pos.CENTER);
        sidePane = gui.getToolbarPane();
        viewHelp = new Button("View Help");
        viewHelp.setMinWidth(178);
        sidePane.getChildren().add(1,viewHelp);
        sidePane.getChildren().add(currentUser);
        counter = new Label("");
        home = new Button("Home");
        home.setMinWidth(178);
        start = new Button("Start Playing");
        start.setMinWidth(178);
        modePage = new Button("Mode Selection");
        //menu~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        selectMode = new Menu("Select Mode");
        easy = new MenuItem("Easy");
        medium = new MenuItem("Medium");
        difficult = new MenuItem("Difficult");
        test = gui.getNewButton();
        create = gui.getSaveButton();
        closeProfile = new Button("Close");
        closeLogin = new Button("Log in");
        closeCreate = new Button("Create New Profile");
        test.setText("Log in / Out");
        test.setMinWidth(178);
        create.setText("Create Profile");
        create.setMinWidth(178);

        //set Main Pane
        quit = new Button("X");
        quit.setStyle("-fx-text-fill: black;-fx-font-size: 15px;-fx-background-color:transparent");
        quit.setMaxHeight(10);
        quit.setMaxWidth(10);
        quit.setAlignment(Pos.TOP_RIGHT);

        mainPane = new Pane();

        mainPane.getChildren().add(quit);
        quit.setLayoutY(0);
        quit.setLayoutX(600);

        appPane = gui.getAppPane();
        appPane.setRight(mainPane);
        workspace = new Pane();

    }
    public Timeline getTimeline(){
        return timeline;
    }
    public void addPersonalBest(int score){
        Label best = new Label("NEW PERSONAL BEST: " + score);
        mainPane.getChildren().add(best);
        best.setLayoutX(50);
        best.setLayoutY(20);
        best.setFont(new Font(40));
    }


    private void setupHandlers() throws IOException{
        quit.setOnMouseClicked( e-> {
                    boolean check = controller.getGameStart();
                    if (check) {
                        boolean exitGame = controller.askToExit();
                        if(exitGame){
                            Platform.exit();
                        }
                    } else {
                        Platform.exit();
                    }
                }

        );

        test.setOnMouseClicked(e -> {
            if(!controller.login) {
                Login();
            }
            else{
                try {
                    controller.login(null,null);
                    OldHome();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });

        create.setOnMouseClicked(e ->createProfile());

        closeLogin.setOnMouseClicked(e -> {
            try {
                loginInfo.close();
                controller.login(userinput.getText(),userPw.getText());
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        });

        closeCreate.setOnMouseClicked( e-> {
            try{
                controller.createProfile(userinput.getText(),userPw.getText());
            }catch(Exception e2){
                e2.printStackTrace();
            }
        });

        closeProfile.setOnMouseClicked( e->
                loginInfo.close()
        );
        home.setOnMouseClicked( e->{
            OldHome();
        });

        start.setOnMouseClicked( e-> {
                    if (timeline != null) {
                        timeline.stop();
                    }
                    displayGame();
                }
        );

        easy.setOnAction( e-> {
                    controller.setCurrentMode("Easy");
                    controller.easyModeScreen("Easy");
                }
        );

        medium.setOnAction( e->{
            controller.setCurrentMode("Medium");
            controller.easyModeScreen("Medium");
        });

        difficult.setOnAction( e->
                {
                    controller.setCurrentMode("Difficult");
                    controller.easyModeScreen("Difficult");
                }
        );

        modePage.setOnMouseClicked( e->
            NewHome()
        );
        currentUser.setOnMouseClicked( e ->

            viewProfile()
        );
        viewHelp.setOnMouseClicked( e->
            helpScreen()
        );
    }

    /**
     * This function specifies the CSS for all the UI components known at the time the workspace is initially
     * constructed. Components added and/or removed dynamically as the application runs need to be set up separately.
     */
    @Override
    public void initStyle() {
        PropertyManager propertyManager = PropertyManager.getManager();

        gui.getAppPane().setId(propertyManager.getPropertyValue(ROOT_BORDERPANE_ID));
        gui.getToolbarPane().getStyleClass().setAll(propertyManager.getPropertyValue(SEGMENTED_BUTTON_BAR));
        gui.getToolbarPane().setId(propertyManager.getPropertyValue(TOP_TOOLBAR_ID));

        ObservableList<Node> toolbarChildren = gui.getToolbarPane().getChildren();
        toolbarChildren.get(0).getStyleClass().add(propertyManager.getPropertyValue(FIRST_TOOLBAR_BUTTON));
        toolbarChildren.get(toolbarChildren.size() - 1).getStyleClass().add(propertyManager.getPropertyValue(LAST_TOOLBAR_BUTTON));

        workspace.getStyleClass().add(CLASS_BORDERED_PANE);
        controller.shortCut();
    }

    /** This function reloads the entire workspace */
    @Override
    public void reloadWorkspace() {
        /* does nothing; use reinitialize() instead */
    }

    public VBox getGameTextsPane() {
        return gameTextsPane;
    }

    public HBox getRemainingGuessBox() {
        return remainingGuessBox;
    }

    public Button getStartGame() {
        return startGame;
    }

    public void reinitialize() {

        guessedLetters = new HBox();
        guessedLetters.setStyle("-fx-background-color: transparent;");
        remainingGuessBox = new HBox();
        gameTextsPane = new VBox();
        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters);

        gameTextsPane.setPadding(new Insets(50,50,50,50));

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(0,10,0,10));

        bodyPane.getChildren().setAll(figurePane, grid);
    }

    private void helpScreen(){

        sidePane.getChildren().remove(0,sidePane.getChildren().size());
        sidePane.getChildren().addAll(guiHeadingLabel,currentUser,home);
        mainPane.getChildren().remove(0,mainPane.getChildren().size());
        mainPane.getChildren().addAll(quit);
        TextArea scrollable = new TextArea(
                "HELP SECTION\n-------------------------------------------------------------------\n" +
                        "Short Cut: \n" +
                        "CONTROL + H: Home Screen\n" +
                        "CONTROL + L: Log In\n" +
                        "CONTROL + Q: Quit the applicaiton\n" +
                        "Game mode:\n" +
                        "Easy:\n " +
                        "Any letter longer than 2 characters \n" +
                        "Level increases with decrease time and increase target point\n" +
                        "Medium: \n" +
                        "Same with easy\n" +
                        "But letter are limited to 4 or more characters \n" +
                        "Hard: \n" +
                        "Letters only allowed for 5 or more Characters.\n" +
                        "Scoring:\n" +
                        "each character * 2 = wroth of each letter\n" +
                        "Formula for scoring:\n" +
                        "Sum of all letters\n" +
                        "Formula for time set for each level\n" +
                        "time = 115 - currentLeve * 5\n"

        );
        scrollable.setFont(new Font(20));
        scrollable.setEditable(false);
        mainPane.getChildren().add(scrollable);
        scrollable.setLayoutX(0);
        scrollable.setLayoutY(20);
        scrollable.setMinHeight(600);
        scrollable.setMaxWidth(600);

    }
    private void viewProfile(){
        loginInfo = new Stage();
        VBox allInfo = new VBox();
        editProfile = new Button("EDIT PROFILE");
        editProfile.setOnMouseClicked( e-> {
            loginInfo.close();
            try {
                changeProfile();
            }catch(Exception e1){

            }
        }
        );
        HBox username = new HBox();
        username.setMinHeight(100);
        username.setMinWidth(300);
        username.setAlignment(Pos.CENTER_LEFT);
        Label user = new Label("Username");
        user.setMinWidth(150);
        user.setStyle("-fx-text-fill: white");
        Label a = new Label(controller.getUsername());
        user.setMinWidth(150);
        a.setStyle("-fx-text-fill: white");
        username.getChildren().addAll(user,a);
        username.setStyle("-fx-background-color: black;");
        username.setPadding(new Insets(0,10,0,10));

        HBox pw = new HBox();
        pw.setMinHeight(50);
        pw.setMinWidth(300);
        pw.setAlignment(Pos.CENTER_LEFT);
        Label pwLabel = new Label("Password");
        pwLabel.setMinWidth(150);
        pwLabel.setStyle("-fx-text-fill: white");
        Label userPw = new Label("************");
        pwLabel.setMinWidth(150);
        userPw.setStyle("-fx-text-fill: white");
        pw.getChildren().addAll(pwLabel,userPw);
        pw.setStyle("-fx-background-color: black;");
        pw.setPadding(new Insets(0,10,0,10));

        HBox closeButton = new HBox();
        closeButton.setSpacing(10);
        closeButton.getChildren().addAll(editProfile,closeProfile);
        closeButton.setAlignment(Pos.BOTTOM_RIGHT);
        closeButton.setPadding(new Insets(0,10,0,10));


        allInfo.getChildren().add(0,username);
        allInfo.getChildren().add(1,pw);
        allInfo.getChildren().add(2,closeButton);
        allInfo.setStyle("-fx-background-color: black");

        Scene dialogScene = new Scene(allInfo,300,200);
        loginInfo.setScene(dialogScene);
        loginInfo.show();

    }

    private void changeProfile() throws Exception{
        loginInfo = new Stage();
        VBox allInfo = new VBox();
        Button submit = new Button("SUBMIT");
        submit.setOnMouseClicked( e->{
            try {
                loginInfo.close();
                controller.changePw(userinput.getText(), userPw.getText());
            }catch(Exception e1){

            }
        }
        );
        HBox username = new HBox();
        username.setMinHeight(100);
        username.setMinWidth(300);
        username.setAlignment(Pos.CENTER_LEFT);
        Label user = new Label("Old Password");
        user.setMinWidth(150);
        user.setStyle("-fx-text-fill: white");
        userinput = new TextField();
        username.getChildren().addAll(user,userinput);
        username.setStyle("-fx-background-color: black;");
        username.setPadding(new Insets(0,10,0,10));

        HBox pw = new HBox();
        pw.setMinHeight(50);
        pw.setMinWidth(300);
        pw.setAlignment(Pos.CENTER_LEFT);
        Label pwLabel = new Label("New Password");
        pwLabel.setMinWidth(150);
        pwLabel.setStyle("-fx-text-fill: white");
        userPw = new TextField();
        pw.getChildren().addAll(pwLabel,userPw);
        pw.setStyle("-fx-background-color: black;");
        pw.setPadding(new Insets(0,10,0,10));

        HBox closeButton = new HBox();
        closeButton.getChildren().addAll(submit);
        closeButton.setAlignment(Pos.BOTTOM_RIGHT);
        closeButton.setPadding(new Insets(0,10,0,10));

        allInfo.getChildren().add(0,username);
        allInfo.getChildren().add(1,pw);
        allInfo.getChildren().add(2,closeButton);
        allInfo.setStyle("-fx-background-color: black");

        Scene dialogScene = new Scene(allInfo,300,200);
        loginInfo.setScene(dialogScene);
        loginInfo.show();
    }

    private void createProfile(){
        loginInfo = new Stage();
        VBox allInfo = new VBox();

        HBox username = new HBox();
        username.setMinHeight(100);
        username.setMinWidth(300);
        username.setAlignment(Pos.CENTER_LEFT);
        Label user = new Label("Profile Name");
        user.setMinWidth(150);
        user.setStyle("-fx-text-fill: white");
        userinput = new TextField();
        username.getChildren().addAll(user,userinput);
        username.setStyle("-fx-background-color: black;");
        username.setPadding(new Insets(0,10,0,10));

        HBox pw = new HBox();
        pw.setMinHeight(50);
        pw.setMinWidth(300);
        pw.setAlignment(Pos.CENTER_LEFT);
        Label pwLabel = new Label("Profile Password");
        pwLabel.setMinWidth(150);
        pwLabel.setStyle("-fx-text-fill: white");
        userPw = new TextField();
        pw.getChildren().addAll(pwLabel,userPw);
        pw.setStyle("-fx-background-color: black;");
        pw.setPadding(new Insets(0,10,0,10));

        HBox closeButton = new HBox();
        closeButton.getChildren().addAll(closeCreate);
        closeButton.setAlignment(Pos.BOTTOM_RIGHT);
        closeButton.setPadding(new Insets(0,10,0,10));

        allInfo.getChildren().add(0,username);
        allInfo.getChildren().add(1,pw);
        allInfo.getChildren().add(2,closeButton);
        allInfo.setStyle("-fx-background-color: black");

        Scene dialogScene = new Scene(allInfo,300,200);
        loginInfo.setScene(dialogScene);
        loginInfo.show();
    }

    public void Login(){
        loginInfo = new Stage();
        VBox allInfo = new VBox();

        HBox username = new HBox();
        username.setMinHeight(100);
        username.setMinWidth(300);
        username.setAlignment(Pos.CENTER_LEFT);
        Label user = new Label("Profile Name");
        user.setMinWidth(150);
        user.setStyle("-fx-text-fill: white");
        userinput = new TextField();
        username.getChildren().addAll(user,userinput);
        username.setStyle("-fx-background-color: black;");
        username.setPadding(new Insets(0,10,0,10));

        HBox pw = new HBox();
        pw.setMinHeight(50);
        pw.setMinWidth(300);
        pw.setAlignment(Pos.CENTER_LEFT);
        Label pwLabel = new Label("Profile Password");
        pwLabel.setMinWidth(150);
        pwLabel.setStyle("-fx-text-fill: white");
        userPw = new TextField();
        pw.getChildren().addAll(pwLabel,userPw);
        pw.setStyle("-fx-background-color: black;");
        pw.setPadding(new Insets(0,10,0,10));

        HBox closeButton = new HBox();
        closeButton.getChildren().addAll(closeLogin);
        closeButton.setAlignment(Pos.BOTTOM_RIGHT);
        closeButton.setPadding(new Insets(0,10,0,10));

        allInfo.getChildren().add(0,username);
        allInfo.getChildren().add(1,pw);
        allInfo.getChildren().add(2,closeButton);
        allInfo.setStyle("-fx-background-color: black");

        Scene dialogScene = new Scene(allInfo,300,200);
        loginInfo.setScene(dialogScene);
        loginInfo.show();
    }

    public void NewHome(){
        currentUser.setDisable(false);

//        startGame.setStyle(
//                "-fx-background-color: grey;-fx-text-fill:lightgray;-fx-padding: 8;-fx-text-alignment: left"
//        );
//
//        startGame.setMinWidth(150);
//        startGame.setMinHeight(10);
//        startGame.setAlignment(Pos.CENTER_LEFT);

        sidePane = gui.getToolbarPane();
        sidePane.getChildren().remove(0,sidePane.getChildren().size());
        if(selectMode.getItems().isEmpty()) {
            selectMode.getItems().addAll(easy, medium, difficult);
            mainmenu = new MenuBar();
            mainmenu.setMinWidth(150);
            mainmenu.getMenus().addAll(selectMode);
        }
        sidePane.getChildren().addAll(guiHeadingLabel,viewHelp,currentUser,home,mainmenu,start);

        start.setDisable(true);
    }

    public void OldHome(){
        controller.setGameStart(false);
        controller.setGameOn(false);
        sidePane = gui.getToolbarPane();
        sidePane.getChildren().remove(0,sidePane.getChildren().size());
        sidePane.getChildren().addAll(guiHeadingLabel,viewHelp,currentUser,test,create);

        mainPane.getChildren().remove(1,mainPane.getChildren().size());
        if(controller.login){
            sidePane.getChildren().add(modePage);
            currentUser.setDisable(false);
        }
        else{
            currentUser.setDisable(true);
        }
    }


    public void displayMode(int maxLevel){
        GridPane letterPane = new GridPane();
        letterPane.setVgap(1);
        letterPane.setHgap(1);
        letterPane.setMinWidth(400);
        letterPane.setMinHeight(300);
        int col = 0;
        int row = 0;
        int numunlocked = 0;

        for(int i =1; i <= 8; i++) {
            final int l = i;
            StackPane newCircle = new StackPane();
            level = new Button();
            Circle one;
            one = new Circle();
            one.setRadius(25);
            level.setMinHeight(50);
            level.setMinWidth(50);
            one.setEffect(new DropShadow(15, Color.BLACK));
            level.setShape(one);
            Text addon = new Text();
            if(numunlocked <= maxLevel ) {
                one.setStyle("-fx-fill: white");
                level.setStyle("-fx-background-color:white");
                addon.setStyle("-fx-fill: black; -fx-font-size: 15");
                level.setOnMouseClicked( e-> {
                            controller.currentLevel = l;
                            start.setDisable(false);
                        }
                );
            }
            else{
                one.setStyle("-fx-fill: gray");
                addon.setStyle("-fx-fill: darkgray; -fx-font-size: 15");
                level.setStyle("-fx-background-color:#6f646b");
            }
            addon.setText(Integer.toString(i));
            newCircle.getChildren().addAll(level);
            newCircle.setPadding(new Insets(10,20,10,20));
            newCircle.getChildren().addAll(addon);
            letterPane.add(newCircle,col,row);
            if(col == 3){
                col = 0;
                row++;
            }
            else{
                col++;
            }
            numunlocked++;
        }

        Label modeLabel = new Label(controller.currentMode);
        modeLabel.setStyle("-fx-text-fill: white;-fx-font-size: 25px;");

        mainPane.getChildren().add(modeLabel);
        modeLabel.setLayoutX(200);
        modeLabel.setLayoutY(100);

        mainPane.getChildren().add(letterPane);
        letterPane.setLayoutX(50);
        letterPane.setLayoutY(140);




    }

    public void findButtonText(int ok){
        int counterx = 0;
        int countery =0;
        for(int i = 0; i < 16; i++){
            int x = i / 4;
            int y = i %4;
                if(wordCircle[i].getEffect() != null && !checkPass[x][y]){
                    counterx = x;
                    countery= y;
                    if((Math.abs(pos-counterx) <= 1) && (Math.abs(posyy - countery) <= 1)) {
                        String temp = controller.getAnswer();
                        controller.setAnswer(temp + lettergrid[x][y]);
                        checkPass[x][y] = true;
                    }
                    else{
                        wordCircle[i].setEffect(null);
                    }
                }
        }
        if(ok == 0){
            pos = counterx;
            posyy = countery;
        }
        else{
            if((Math.abs(pos-counterx) <= 1) && (Math.abs(posyy - countery) <= 1)) {
                DropShadow dropShadow = new DropShadow();
                dropShadow.setRadius(10.0);
                dropShadow.setOffsetX(3.0);
                dropShadow.setOffsetY(3.0);
                dropShadow.setColor(Color.RED);
                int zz = controller.findtheLine(pos, posyy, counterx, countery);
                line[zz].setEffect(dropShadow);
                pos = counterx;
                posyy = countery;
            }
        }


    }
    public void displayGame(){
        mainPane.getChildren().remove(1,mainPane.getChildren().size());

        sidePane.getChildren().remove(0,sidePane.getChildren().size());

        sidePane.getChildren().addAll(guiHeadingLabel,viewHelp,currentUser,home);
        GridPane letterPane = new GridPane();
        letterPane.setVgap(1);
        letterPane.setHgap(1);
        letterPane.setMaxWidth(400);
        letterPane.setMaxHeight(300);
        int col = 0;
        int row = 0;
        int hx = 0;
        int hy= 0;
        wordCircle= new StackPane[16];
        buttons = new Button[4][4];
        DropShadow dropShadow= new DropShadow();
        dropShadow.setRadius(10.0);
        dropShadow.setOffsetX(3.0);
        dropShadow.setOffsetY(3.0);
        dropShadow.setColor(Color.RED);
        for(int i =0; i < 16; i++) {
            StackPane newCircle = new StackPane();
            newCircle.setMinHeight(30);
            newCircle.setMinWidth(30);
            wordCircle[i] = newCircle;
            Button letterButton = new Button();
//            newCircle.setMouseTransparent(true);

            buttons[hx][hy++] = letterButton;
            if(hy == 4){
                hx++;
                hy =0;
            }

            newCircle.setOnDragDetected( e->
                    {
                        for(int z = 0; z < 16; z++){
                            wordCircle[z].setEffect(null);
                        }
                        for(int z = 0; z < 42; z++){
                            line[z].setEffect(null);
                        }
                        if(controller.getGameStart() && controller.getGameOn()) {
                            newCircle.setEffect(dropShadow);
                            newCircle.startFullDrag();
                            controller.setAnswer("");
                            checkPass = new boolean[4][4];
                            pos = -1;
                            posyy = -1;
                            findButtonText(0);
                        }
                    }
            );
            newCircle.setOnMouseDragEntered( e->
                    {


                        newCircle.setEffect(dropShadow);
                        findButtonText(1);


                    }
            );
            newCircle.setOnMouseDragReleased( e->
                    {

                        if(controller.getAnswerList().contains(controller.getAnswer()) && !controller.getGuessedList().contains(controller.getAnswer())){
                            controller.getGuessedList().add(controller.getAnswer());
                            getGuessedWordBox().appendText(
                                    controller.getAnswer() + "\t" + Integer.toString(controller.getAnswer().length() * 2) + "\n");
                            addCurrentPoint(controller.getAnswer().length() * 2);
                        }
                        controller.setAnswer("");

                        for(int z = 0; z < 16; z++){
                            wordCircle[z].setEffect(null);
                        }
                        for(int z = 0; z < 42; z++){
                            line[z].setEffect(null);
                        }
                    }
            );

            Circle one = new Circle();
            letterButton.setMinHeight(50);
            letterButton.setMinWidth(50);
            letterButton.setStyle("-fx-background-color:white");
            one.setStyle("-fx-fill: white");
            letterButton.setShape(one);
            one.setRadius(25);
            newCircle.setShape(one);
            newCircle.getChildren().add(letterButton);
            newCircle.setPadding(new Insets(10,20,10,20));

            letterPane.add(newCircle,col,row);



            if(col == 3){
                col = 0;
                row++;
            }
            else{
                col++;
            }
        }
        line = new Line[44];
        int hY = 175;
        int hX = 110;
        for(int i = 0; i < 12; i++) {
            Line HLine = new Line();
            line[i] = HLine;
            if(hY >= 175+70*4){
                hY = 175;
                hX += 95;
            }
            HLine.setStartX(hX);
            HLine.setStartY(hY);
            HLine.setEndX(hX + 95);
            HLine.setEndY(hY);
            HLine.setStyle("-fx-stroke: #6f646b;-fx-stroke-width: 3px");
            hY+=70;
            mainPane.getChildren().add(HLine);
//            if(i == 3) {
//                DropShadow dropShadow = new DropShadow();
//                dropShadow.setRadius(10.0);
//                dropShadow.setOffsetX(3.0);
//                dropShadow.setOffsetY(3.0);
//                dropShadow.setColor(Color.RED);
//                HLine.setEffect(dropShadow);
//            }
        }
        hY = 175;
        hX = 130;
        for(int i = 12; i < 24; i++){
            Line Vline = new Line();
            line[i] = Vline;
            if(hY >= 175 + 70*3){
                hX += 90;
                hY = 175;
            }
            Vline.setStartX(hX);
            Vline.setStartY(hY);
            Vline.setEndX(hX);
            Vline.setEndY(hY+70);
            Vline.setStyle("-fx-stroke: #6f646b;-fx-stroke-width: 3px");
            hY += 70;
            mainPane.getChildren().add(Vline);

        }
        hY = 175;
        hX = 130;
        for(int i = 24; i < 33; i++){
            Line HLine = new Line();
            line[i] = HLine;
            if(hY >= 175+70*3){
                hY = 175;
                hX += 90;
            }
            HLine.setStartX(hX);
            HLine.setStartY(hY);
            HLine.setEndX(hX + 95);
            HLine.setEndY(hY + 70);
            HLine.setStyle("-fx-stroke: #6f646b;-fx-stroke-width: 3px");
            hY+=70;
            mainPane.getChildren().add(HLine);

        }

        hY = 175+ 70;
        hX = 130;
        for(int i = 33; i < 42; i++){
            Line HLine = new Line();
            line[i] = HLine;
            if(hY >= 175+70*4){
                hY = 175 + 70;
                hX += 90;
            }
            HLine.setStartX(hX);
            HLine.setStartY(hY);
            HLine.setEndX(hX + 95);
            HLine.setEndY(hY - 70);
            HLine.setStyle("-fx-stroke: #6f646b;-fx-stroke-width: 3px");
            hY+=70;
            mainPane.getChildren().add(HLine);

        }

        replay = new Button("REPLAY");
        replay.setLayoutX(50);
        replay.setLayoutY(500);
        replay.setDisable(true);
        replay.setOnMouseClicked( e->{
            displayGame();
            if(timeline != null){
                timeline.stop();
            }
                lettergrid = controller.findWords(controller.setUpLetter());
        int target = controller.getTargetPoint();

        targetPointLabel.setText("Target Point: " + target);
        buildLetterGrid();
        Pause.setText("Pause");
        controller.setGameOn(true);
        controller.setGameStart(true);

        gameDuration = 120 - controller.currentLevel*5;

        timeline = new Timeline();
        gameDurationInInteger = new SimpleIntegerProperty(gameDuration);
        gameDurationInInteger.set(gameDuration);
        counter.textProperty().bind(gameDurationInInteger.asString());
        timeline.getKeyFrames().add(new KeyFrame(Duration.millis(gameDuration*1000), new KeyValue(gameDurationInInteger, 0)));
        timeline.playFromStart();

        timeline.setOnFinished( event->{
                    //Fronze game screen
                    controller.gameOver();
                    controller.setGameOn(false);
                    controller.setGameStart(false);
                    Pause.setDisable(true);
                    //restart and replay button
                }
        );
        controller.play();
    });
        mainPane.getChildren().add(replay);
        toNext = new Button("NEXT LEVEL");
        toNext.setLayoutX(350);
        toNext.setLayoutY(500);
        save = new Button("SAVE");
        save.setLayoutX(50);
        save.setLayoutY(550);
        save.setOnMouseClicked(e->
            save.setDisable(true)
        );
        toNext.setOnMouseClicked( e-> {

            if(timeline != null){
                timeline.stop();
            }
            controller.setCurrentLevel();
            displayGame();
            lettergrid = controller.findWords(controller.setUpLetter());
            int target = controller.getTargetPoint();

            targetPointLabel.setText("Target Point: " + target);
            buildLetterGrid();
            Pause.setText("Pause");
            controller.setGameOn(true);
            controller.setGameStart(true);

            gameDuration = 120 - controller.currentLevel*5;

            timeline = new Timeline();
            gameDurationInInteger = new SimpleIntegerProperty(gameDuration);
            gameDurationInInteger.set(gameDuration);
            counter.textProperty().bind(gameDurationInInteger.asString());
            timeline.getKeyFrames().add(new KeyFrame(Duration.millis(gameDuration*1000), new KeyValue(gameDurationInInteger, 0)));
            timeline.playFromStart();

            timeline.setOnFinished( event->{
                        //Fronze game screen
                        controller.gameOver();
                        controller.setGameOn(false);
                        controller.setGameStart(false);
                        Pause.setDisable(true);
                        //restart and replay button
                    }
            );
            controller.play();
        });
//        Line HLine1 = new Line();
//        HLine1.setStartX(110);
//        HLine1.setStartY(245);
//        HLine1.setEndX(400);
//        HLine1.setEndY(245);
//        HLine1.setStyle("-fx-stroke: #6f646b;-fx-stroke-width: 3px");
//
//        Line HLine2 = new Line();
//        HLine2.setStartX(110);
//        HLine2.setStartY(320);
//        HLine2.setEndX(400);
//        HLine2.setEndY(320);
//        HLine2.setStyle("-fx-stroke: #6f646b;-fx-stroke-width: 3px");
//
//        Line HLine3 = new Line();
//        HLine3.setStartX(110);
//        HLine3.setStartY(390);
//        HLine3.setEndX(400);
//        HLine3.setEndY(390);
//        HLine3.setStyle("-fx-stroke: #6f646b;-fx-stroke-width: 3px");
//
//
//        Line VLine = new Line();
//        VLine.setStartX(135);
//        VLine.setStartY(175);
//        VLine.setEndX(135);
//        VLine.setEndY(390);
//        VLine.setStyle("-fx-stroke: #6f646b;-fx-stroke-width: 3px");
//
//        Line VLine1 = new Line();
//        VLine1.setStartX(225);
//        VLine1.setStartY(175);
//        VLine1.setEndX(225);
//        VLine1.setEndY(390);
//        VLine1.setStyle("-fx-stroke: #6f646b;-fx-stroke-width: 3px");
//
//        Line VLine2 = new Line();
//        VLine2.setStartX(315);
//        VLine2.setStartY(175);
//        VLine2.setEndX(315);
//        VLine2.setEndY(390);
//        VLine2.setStyle("-fx-stroke: #6f646b;-fx-stroke-width: 3px");
//
//        Line VLine3 = new Line();
//        VLine3.setStartX(405);
//        VLine3.setStartY(175);
//        VLine3.setEndX(405);
//        VLine3.setEndY(390);
//        VLine3.setStyle("-fx-stroke: #6f646b;-fx-stroke-width: 3px");

        remainingTime = new Label("TIME REMAINING:                  seconds");
        remainingTime.setStyle("-fx-text-fill: red;-fx-background-color: lightgrey;");
        remainingTime.setPadding(new Insets(10,4,10,4));

        counter.setStyle("-fx-text-fill: red;-fx-background-color: lightgrey;");

        Pause = new Button("START");

        Pause.setOnMouseClicked( e-> {
                    if(!controller.checkCurrentGame() && !controller.getGameStart()) {
                        replay.setDisable(false);
                        lettergrid = controller.findWords(controller.setUpLetter());
                        int target = controller.getTargetPoint();

                        targetPointLabel.setText("Target Point: " + target);
                        buildLetterGrid();
                        Pause.setText("Pause");
                        controller.setGameOn(true);
                        controller.setGameStart(true);

                        gameDuration = 120 - controller.currentLevel*5;

                        timeline = new Timeline();
                        gameDurationInInteger = new SimpleIntegerProperty(gameDuration);
                        gameDurationInInteger.set(gameDuration);
                        counter.textProperty().bind(gameDurationInInteger.asString());
                        timeline.getKeyFrames().add(new KeyFrame(Duration.millis(gameDuration*1000), new KeyValue(gameDurationInInteger, 0)));
                        timeline.playFromStart();

                        timeline.setOnFinished( event->{
                                    //Fronze game screen
                                    controller.gameOver();
                                    controller.setGameOn(false);
                                    controller.setGameStart(false);
                                    Pause.setDisable(true);
                                    //restart and replay button
                                }
                        );
                        controller.play();
                    }

                    else if(controller.getGameStart() && !controller.checkCurrentGame()){
                        replay.setDisable(false);

                        buildLetterGrid();
                        Pause.setText("Pause");
                        controller.setGameOn(true);
                        timeline.play();
                    }
                    else{
                        controller.setGameOn(false);
                        Pause.setText("Continue");
                        pauseReaction();
                        timeline.stop();
                        replay.setDisable(true);

                    }

                }
        );

        Label modeLabel = new Label(controller.currentMode);
        mainPane.getChildren().add(modeLabel);
        modeLabel.setLayoutX(220);
        modeLabel.setLayoutY(100);
        modeLabel.setStyle("-fx-text-fill: white;-fx-font-size: 25px;");


        Label levelLabel = new Label("Level " + controller.currentLevel);
        levelLabel.setStyle("-fx-text-fill: white;-fx-font-size: 25px;-fx-underline: true");

        HBox guessingBox = new HBox();
        guessingLabel = new Text();
        guessingBox.setAlignment(Pos.CENTER);
        guessingLabel.setFont(new Font(20));
        guessingBox.getChildren().add(guessingLabel);
        guessingBox.setStyle("-fx-background-color: rgba(45, 36, 38, 0.55)");

        StackPane guessingWord = new StackPane();
        guessingBox.getChildren().add(guessingWord);
        guessingBox.setMinHeight(40);
        guessingBox.setMinWidth(140);

        VBox targetPoint = new VBox();
        targetPoint.setMinWidth(140);
        targetPoint.setMinHeight(40);
        targetPoint.setStyle("-fx-background-color: rgba(45, 36, 38, 0.55);");
        targetPoint.setPadding(new Insets(10,5,10,5));


        targetPointLabel = new Label("Target Point:  ");
        targetPointLabel.setStyle("-fx-text-fill: white");
        targetPoint.getChildren().addAll(targetPointLabel);

        VBox largeGuessedBox = new VBox();
        largeGuessedBox.setMinHeight(220);
        largeGuessedBox.setMinWidth(140);
        largeGuessedBox.setStyle("-fx-background-color: rgba(45, 36, 38, 0.55)");

        guessedWordBox = new TextArea();
        guessedWordBox.setEditable(false);

        guessedWordBox.setMaxHeight(190);
        guessedWordBox.setMaxWidth(140);

        guessedWordBox.setStyle("-fx-border-width: 1px;-fx-border-color: black");

        Label totalPoint = new Label(" TOTAL POINT: ");
        totalPoint.setStyle("-fx-background-color:rgba(0, 0, 0, 0.84);-fx-text-fill: white");
        totalPoint.setMinWidth(140);
        totalPoint.setMinHeight(30);

        currentPoint = new Label("0");
        currentPoint.setStyle("-fx-background-color:rgba(0, 0, 0, 0.84);-fx-text-fill: white");
        currentPoint.setMinWidth(140);
        currentPoint.setMinHeight(30);
        currentPoint.setPadding(new Insets(0,0,5,50));

        smallGuessedBox = new VBox();
        smallGuessedBox.getChildren().addAll(guessedWordBox);

        largeGuessedBox.getChildren().add(0,smallGuessedBox);
        largeGuessedBox.getChildren().add(1,totalPoint);
        largeGuessedBox.getChildren().add(2,currentPoint);

        mainPane.getChildren().add(remainingTime);
        remainingTime.setLayoutX(430);
        remainingTime.setLayoutY(90);

        mainPane.getChildren().add(counter);
        counter.setLayoutX(580);
        counter.setLayoutY(100);

        mainPane.getChildren().add(Pause);
        Pause.setLayoutX(230);
        Pause.setLayoutY(500);

        mainPane.getChildren().add(levelLabel);
        levelLabel.setLayoutX(220);
        levelLabel.setLayoutY(440);

        mainPane.getChildren().add(guessingBox);
        guessingBox.setLayoutX(480);
        guessingBox.setLayoutY(150);

        mainPane.getChildren().add(targetPoint);
        targetPoint.setLayoutX(480);
        targetPoint.setLayoutY(500);

        mainPane.getChildren().add(largeGuessedBox);
        largeGuessedBox.setLayoutX(480);
        largeGuessedBox.setLayoutY(200);

//        mainPane.getChildren().add(HLine1);
//        mainPane.getChildren().add(HLine2);
//        mainPane.getChildren().add(HLine3);
//        mainPane.getChildren().add(VLine);
//        mainPane.getChildren().add(VLine1);
//        mainPane.getChildren().add(VLine2);
//        mainPane.getChildren().add(VLine3);

        mainPane.getChildren().add(letterPane);
        letterPane.setLayoutX(87);
        letterPane.setLayoutY(140);

    }

    public void buildLetterGrid(){
        letterPane = new GridPane();
        letterPane.setVgap(1);
        letterPane.setHgap(1);
        letterPane.setMinWidth(400);
        letterPane.setMinHeight(300);

        int col = 0;
        int row = 0;
        int hx= 0;
        int hy =0;
        for(int i =0; i < 16; i++) {
            StackPane gg = wordCircle[i] ;
//            Button letterButton = new Button();
//            if(cu > 3){
//                cu = 0;
//                cc++;
//            }
//            buttons[cc][cu++] = letterButton;
//            Circle one;
//            letterButton.setMinHeight(50);
//            letterButton.setMinWidth(50);
//            one.setStyle("-fx-fill: white");
//            letterButton.setShape(one);

            int x = i / 4;
            int y = i % 4;
            displayLetter = new Text(Character.toString(lettergrid[x][y]));
            displayLetter.setStyle("-fx-font-size: 20px");
//            gg.getChildren().add(buttons[hx][hy++]);
//            if(hy ==4){
//                hx++;
//                hy = 0;
//            }

            gg.getChildren().add(displayLetter);

            gg.setPadding(new Insets(10,20,10,20));

            letterPane.add(gg,col,row);

            if(col == 3){
                col = 0;
                row++;
            }
            else{
                col++;
            }
        }
        mainPane.getChildren().add(letterPane);
        letterPane.setLayoutX(87);
        letterPane.setLayoutY(140);

    }

    public void pauseReaction(){
        letterPane.setVisible(false);
        GridPane letterPane = new GridPane();
        letterPane.setVgap(1);
        letterPane.setHgap(1);
        letterPane.setMinWidth(400);
        letterPane.setMinHeight(300);
        int col = 0;
        int row = 0;
        for(int i =0; i < 16; i++) {
            StackPane wordCircle = new StackPane();
            Circle one;
            one = new Circle();
            one.setRadius(25);
            one.setStyle("-fx-fill: white");
            wordCircle.getChildren().add(one);

            wordCircle.setPadding(new Insets(10,20,10,20));

            letterPane.add(wordCircle,col,row);
            if(col == 3){
                col = 0;
                row++;
            }
            else{
                col++;
            }
        }
        mainPane.getChildren().add(letterPane);
        letterPane.setLayoutX(87);
        letterPane.setLayoutY(140);
    }
}
