package data;

import apptemplate.AppTemplate;
import components.AppDataComponent;
import controller.GameError;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.stream.Stream;

/**
 * @author Ritwik Banerjee
 */
public class GameData implements AppDataComponent {

    public static final  int TOTAL_NUMBER_OF_GUESSES_ALLOWED = 10;
    private static final int TOTAL_NUMBER_OF_STORED_WORDS    = 330622;

    private String         username;
    private String         pw;
    private int            maxLevel;
    private int            MmaxLevel;
    private int            DmaxLevel;

    private int[]          ePersonalBest;
    private int[]          mPersonalBest;
    private int[]          dPersonalBest;
    private String         targetWord;
    private Set<Character> goodGuesses;
    private Set<Character> badGuesses;
    private int            remainingGuesses;
    public  AppTemplate    appTemplate;
    private boolean        yesHint;

    public GameData(AppTemplate appTemplate) {
        this(appTemplate, false);
    }

    public GameData(AppTemplate appTemplate, boolean initiateGame) {
        if (initiateGame) {
            this.appTemplate = appTemplate;
            this.targetWord = setTargetWord();
            this.goodGuesses = new HashSet<>();
            this.badGuesses = new HashSet<>();
            this.remainingGuesses = TOTAL_NUMBER_OF_GUESSES_ALLOWED;
            this.yesHint = false;
        } else {
            this.appTemplate = appTemplate;
        }
    }

    public void setePersonalBest(int level, int score){
        ePersonalBest[level] = score;
    }
    public void setmPersonalBest(int level, int score){
        mPersonalBest[level] = score;
    }
    public void setdPersonalBest(int level, int score){
        dPersonalBest[level] = score;
    }
    public int[] getePersonalBest(){
        return ePersonalBest;
    }
    public int[] getmPersonalBest(){
        return mPersonalBest;
    }
    public int[] getdPersonalBest(){
        return dPersonalBest;
    }

    public void initPersonalBest(){
        dPersonalBest = new int[8];
        mPersonalBest = new int[8];
        ePersonalBest = new int[8];
    }
    public void setMaxLevel(int level){
        this.maxLevel = level;
    }

    public int getMaxLevel(){
        return maxLevel;
    }

    public String getUsername(){
        return username;
    }

    public String getPw(){
        return pw;
    }

    public void setUsername(String username){
        this.username = username;
    }

    public void setPw(String pw){
        this.pw = pw;
    }

    public void setMmaxLevel(int level){this.MmaxLevel = level;}

    public void setDmaxLevel(int level){this.DmaxLevel = level;}

    public int getMmaxLevel(){return MmaxLevel;}

    public int getDmaxLevel(){ return DmaxLevel;}

    public int getePersonalBest(int x){
        return getePersonalBest()[x];
    }

    public void init() {
        this.targetWord = setTargetWord();
        this.goodGuesses = new HashSet<>();
        this.badGuesses = new HashSet<>();
        this.remainingGuesses = TOTAL_NUMBER_OF_GUESSES_ALLOWED;
        this.yesHint = false;
    }

    @Override
    public void reset() {
        this.maxLevel = 0;
        this.username = null;
        this.pw = null;
        this.ePersonalBest = null;
        this.mPersonalBest = null;
        this.dPersonalBest = null;
        this.MmaxLevel = 0;
        this.DmaxLevel = 0;
        appTemplate.getWorkspaceComponent().reloadWorkspace();
    }

    public String getTargetWord() {
        return targetWord;
    }

    private String setTargetWord() {
        URL wordsResource = getClass().getClassLoader().getResource("words/words.txt");
        assert wordsResource != null;

        int toSkip = new Random().nextInt(TOTAL_NUMBER_OF_STORED_WORDS);
        try (Stream<String> lines = Files.lines(Paths.get(wordsResource.toURI()))) {
            return lines.skip(toSkip).findFirst().get();
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }

        throw new GameError("Unable to load initial target word.");
    }

    public GameData setTargetWord(String targetWord) {
        this.targetWord = targetWord;
        return this;
    }

    public Set<Character> getGoodGuesses() {
        return goodGuesses;
    }

    public GameData setGoodGuesses(Set<Character> goodGuesses) {
        this.goodGuesses = goodGuesses;
        return this;
    }

    public Set<Character> getBadGuesses() {
        return badGuesses;
    }

    public GameData setBadGuesses(Set<Character> badGuesses) {
        this.badGuesses = badGuesses;
        return this;
    }

    public int getRemainingGuesses() {
        return remainingGuesses;
    }

    public void addGoodGuess(char c) {
        goodGuesses.add(c);
    }

    public void addBadGuess(char c) {
        if (!badGuesses.contains(c)) {
            badGuesses.add(c);
            remainingGuesses--;
        }
    }

    public boolean isYesHint() {
        return yesHint;
    }

    public void setYesHint(boolean yesHint) {
        this.yesHint = yesHint;
    }
}
