package data;

import com.fasterxml.jackson.core.*;
import components.AppDataComponent;
import components.AppFileComponent;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Set;

/**
 * @author Ritwik Banerjee
 */
public class GameDataFile implements AppFileComponent {

    public static final String TARGET_WORD  = "TARGET_WORD";
    public static final String GOOD_GUESSES = "GOOD_GUESSES";
    public static final String BAD_GUESSES  = "BAD_GUESSES";
    public static final String YES_HINT     = "YES_HINT";

    public static final String USERNAME     = "USERNAME";
    public static final String PASSWORD     = "PASSWORD";
    public static final String DICTIONARY     = "DICTIONARY";
    public static final String DEBEST        = "EASY";
    public static final String MEDIM        = "DICTIONARY2";
    public static final String MBEST        = "MEDIUM";
    public static final String DIFF     = "DICTIONARY3";
    public static final String DBEST    = "DIFFICULT";
    @Override
    public void saveData(AppDataComponent data, Path to) {
        GameData       gamedata    = (GameData) data;

        JsonFactory jsonFactory = new JsonFactory();

        try (OutputStream out = Files.newOutputStream(to)) {

            JsonGenerator generator = jsonFactory.createGenerator(out, JsonEncoding.UTF8);

            generator.writeStartObject();

            generator.writeStringField(USERNAME, gamedata.getUsername());

            generator.writeStringField(PASSWORD, gamedata.getPw());

            generator.writeStringField(DICTIONARY, Integer.toString(gamedata.getMaxLevel()));

            generator.writeFieldName(DEBEST);

            generator.writeStartArray(gamedata.getePersonalBest().length);
            for (int c : gamedata.getePersonalBest())
                generator.writeNumber(c);
            generator.writeEndArray();

            generator.writeStringField(MEDIM, Integer.toString(gamedata.getMmaxLevel()));

            generator.writeFieldName(MBEST);

            generator.writeStartArray(gamedata.getmPersonalBest().length);
            for (int c : gamedata.getmPersonalBest())
                generator.writeNumber(c);
            generator.writeEndArray();

            generator.writeStringField(DIFF, Integer.toString(gamedata.getDmaxLevel()));

            generator.writeFieldName(DBEST);

            generator.writeStartArray(gamedata.getdPersonalBest().length);
            for (int c : gamedata.getdPersonalBest())
                generator.writeNumber(c);
            generator.writeEndArray();

            generator.writeEndObject();

            generator.close();

        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    @Override
    public void loadData(AppDataComponent data, Path from) throws IOException {
        GameData gamedata = (GameData) data;
        gamedata.reset();

        JsonFactory jsonFactory = new JsonFactory();
        JsonParser  jsonParser  = jsonFactory.createParser(Files.newInputStream(from));
        gamedata.initPersonalBest();
        while (!jsonParser.isClosed()) {
            JsonToken token = jsonParser.nextToken();
            if (JsonToken.FIELD_NAME.equals(token)) {
                String fieldname = jsonParser.getCurrentName();
                switch (fieldname) {
                    case USERNAME:
                        jsonParser.nextToken();
                        gamedata.setUsername(jsonParser.getValueAsString());
                        break;
                    case PASSWORD:
                        jsonParser.nextToken();
                        gamedata.setPw(jsonParser.getValueAsString());
                        break;
                    case DICTIONARY:
                        jsonParser.nextToken();
                        gamedata.setMaxLevel(jsonParser.getValueAsInt());
                        break;
                    case DEBEST:
                        jsonParser.nextToken();
                        int counter = 0;
                        while(jsonParser.nextToken() != JsonToken.END_ARRAY){
                            gamedata.setePersonalBest(counter++, jsonParser.getValueAsInt());
                        }
                        break;
                    case MEDIM:
                        jsonParser.nextToken();
                        gamedata.setMmaxLevel(jsonParser.getValueAsInt());
                        break;
                    case MBEST:
                        jsonParser.nextToken();
                        int counter2 = 0;
                        while(jsonParser.nextToken() != JsonToken.END_ARRAY){
                            gamedata.setmPersonalBest(counter2++, jsonParser.getValueAsInt());
                        }
                        break;
                    case DIFF:
                        jsonParser.nextToken();
                        gamedata.setDmaxLevel(jsonParser.getValueAsInt());
                        break;
                    case DBEST:
                        jsonParser.nextToken();
                        int counter3 = 0;
                        while(jsonParser.nextToken() != JsonToken.END_ARRAY){
                            gamedata.setdPersonalBest(counter3++, jsonParser.getValueAsInt());
                        }
                        break;
                    default:
                        throw new JsonParseException(jsonParser, "Unable to load JSON data");
                }
            }

        }
    }

    /** This method will be used if we need to export data into other formats. */
    @Override
    public void exportData(AppDataComponent data, Path filePath) throws IOException { }
}
